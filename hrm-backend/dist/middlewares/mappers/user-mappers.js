export var responseMapper = function (_a) {
    var content = _a.content, message = _a.message;
    return ({
        content: content,
        message: message,
    });
};
export var getAllUsersContentMapper = function (_a) {
    var id = _a.id, first_name = _a.first_name, last_name = _a.last_name, email = _a.email, phone_number = _a.phone_number, access_token = _a.access_token, password = _a.password, role_id = _a.role_id, created_at = _a.created_at, last_updated_at = _a.last_updated_at, role_name = _a.role_name;
    return {
        id: id,
        firstName: first_name,
        lastName: last_name,
        email: email,
        phoneNumber: phone_number,
        role: role_name,
        createdAt: created_at,
        lastUpdatedAt: last_updated_at,
    };
};
