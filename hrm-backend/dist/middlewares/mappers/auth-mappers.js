export var responseMapper = function (_a) {
    var content = _a.content, message = _a.message;
    return ({
        content: content,
        message: message,
    });
};
export var loginContentMapper = function (_a) {
    var accessToken = _a.accessToken, refreshToken = _a.refreshToken;
    return ({
        accessToken: accessToken,
        refreshToken: refreshToken,
    });
};
export var createPassContentMapper = function (_a) {
    var _b = _a[0], id = _b.id, first_name = _b.first_name, last_name = _b.last_name, email = _b.email, phone_number = _b.phone_number, access_token = _b.access_token, password = _b.password, role_id = _b.role_id, created_at = _b.created_at, last_updated_at = _b.last_updated_at, role_name = _b.role_name;
    return {
        id: id,
        firstName: first_name,
        lastName: last_name,
        email: email,
        phoneNumber: phone_number,
        role: role_name,
    };
};
