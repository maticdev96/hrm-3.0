import jwt from 'jsonwebtoken';
var jwtTokens = function (loggedUser) {
    var _a, _b, _c, _d;
    var accessToken = '';
    var refreshToken = '';
    if (process.env.ACCESS_TOKEN_SECRET) {
        accessToken = jwt.sign(loggedUser, (_b = (_a = process === null || process === void 0 ? void 0 : process.env) === null || _a === void 0 ? void 0 : _a.ACCESS_TOKEN_SECRET) === null || _b === void 0 ? void 0 : _b.toString(), { expiresIn: '60m' });
    }
    if (process.env.REFRESH_TOKEN_SECRET) {
        refreshToken = jwt.sign(loggedUser, (_d = (_c = process === null || process === void 0 ? void 0 : process.env) === null || _c === void 0 ? void 0 : _c.REFRESH_TOKEN_SECRET) === null || _d === void 0 ? void 0 : _d.toString(), { expiresIn: '120m' });
    }
    return { accessToken: accessToken, refreshToken: refreshToken };
};
export { jwtTokens };
