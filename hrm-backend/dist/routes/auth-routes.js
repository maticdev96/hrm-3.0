import express from 'express';
import auth from "../controllers/auth-controllers.js";
var router = express.Router();
router.put('/create-password/:id', auth.createPass);
router.post('/login', auth.login);
export default router;
