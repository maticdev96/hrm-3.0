INSERT INTO public."roles" (id, name) VALUES (1, 'admin');
INSERT INTO public."roles" (id, name) VALUES (2, 'hr');
INSERT INTO public."roles" (id, name) VALUES (3, 'employee');

INSERT INTO public."users" (id, first_name, last_name, email, phone_number, access_token, password, role_id, created_at, last_updated_at) VALUES (1, 'Stefan', 'Matić', 'stefan.matic@atessoft.rs', '0645454544', null, null, 1, '2022-09-05 10:50:46.000000', '2022-09-05 10:50:39.000000');
