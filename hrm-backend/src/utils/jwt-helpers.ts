import { IMappedUserResponse } from './../middlewares/interfaces/user-interfaces.js';
import jwt from 'jsonwebtoken';

const jwtTokens = (loggedUser: IMappedUserResponse) => {
  let accessToken = '';
  let refreshToken = '';
  if (process.env.ACCESS_TOKEN_SECRET) {
    accessToken = jwt.sign(
      loggedUser,
      process?.env?.ACCESS_TOKEN_SECRET?.toString(),
      { expiresIn: '60m' }
    );
  }
  if (process.env.REFRESH_TOKEN_SECRET) {
    refreshToken = jwt.sign(
      loggedUser,
      process?.env?.REFRESH_TOKEN_SECRET?.toString(),
      { expiresIn: '120m' }
    );
  }
  return { accessToken, refreshToken };
};

export { jwtTokens };
