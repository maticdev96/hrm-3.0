import db from '../hrm-db/index.js';

export const findRoleById = async (id: number) => {
  const roleName = await db?.pool?.query('SELECT * FROM roles WHERE id = $1', [
    id,
  ]);
  console.log(roleName?.rows[0]?.name);
  return roleName.rows[0]?.name;
};
