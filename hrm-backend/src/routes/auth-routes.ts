import express from 'express';
import auth from '../controllers/auth-controllers.js';

const router = express.Router();

router.put('/create-password/:id', auth.createPass);

router.post('/login', auth.login);

export default router;
