import users from '../controllers/user-controllers.js';
import express from 'express';

const router = express.Router();

router.get('/users', users.get);

export default router;
