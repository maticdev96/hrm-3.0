import { IGetAllUsersContent } from '../middlewares/interfaces/user-interfaces.js';
import {
  getAllUsersContentMapper,
  responseMapper,
} from '../middlewares/mappers/user-mappers.js';
import db from '../hrm-db/index.js';
import { Response } from 'express';

export const getAllUsers = async (res: Response) => {
  const users = await db?.pool?.query(
    'SELECT U.*, R.name AS role_name FROM users U INNER JOIN roles R ON U.role_id = R.id'
  );
  if (users.rows) {
    if (users.rows.length === 0) {
      res.status(404);
      return { error: 'There are no users' };
    }
  } else {
    res.status(500);
    return { error: 'Error while trying to connect to the database' };
  }
  return responseMapper({
    content: users?.rows?.map((row: IGetAllUsersContent) =>
      getAllUsersContentMapper(row)
    ),
  });
};
