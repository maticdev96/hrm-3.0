import { getAllUsersContentMapper } from './../middlewares/mappers/user-mappers.js';
import {
  createPassContentMapper,
  loginContentMapper,
} from '../middlewares/mappers/auth-mappers.js';
import { Request, Response } from 'express';
import db from '../hrm-db/index.js';
import bcrypt from 'bcryptjs';
import { jwtTokens } from '../utils/jwt-helpers.js';
import { responseMapper } from '../middlewares/mappers/auth-mappers.js';
import { loginSchema } from '../middlewares/schemas/auth-schemas.js';

export const createPassword = async (req: Request) => {
  const { id } = req.params;
  const { password } = req.body;
  const hashedPassword = await bcrypt.hash(password, 10);
  await db?.pool?.query(
    'UPDATE users SET password = $1 WHERE id = $2 RETURNING *',
    [hashedPassword, id]
  );
  const updatedUser = await db?.pool?.query(
    'SELECT U.*, R.name AS role_name FROM users U INNER JOIN roles R ON U.role_id = R.id WHERE U.id = $1',
    [id]
  );
  return responseMapper({
    content: createPassContentMapper(updatedUser?.rows),
    message: 'You have successfully registered to HRM App!',
  });
};

export const loginUser = async (req: Request, res: Response) => {
  const { email, password } = req.body;
  const validator = loginSchema.validate(req.body);
  if (validator.error) {
    res.status(400);
    return { error: validator?.error?.message };
  }
  await db.pool.query('SELECT * FROM users WHERE email = $1', [email]);

  const users = await db?.pool?.query(
    'SELECT U.*, R.name AS role_name FROM users U INNER JOIN roles R ON U.role_id = R.id WHERE email = $1',
    [email]
  );
  if (users.rows.length === 0) {
    res.status(401);
    return { error: 'Email is incorrect' };
  }
  const validPassword = await bcrypt.compare(password, users.rows[0].password);
  if (!validPassword) {
    res.status(401);
    return { error: 'Password is incorrect' };
  }
  let tokens = jwtTokens(getAllUsersContentMapper(users.rows[0]));
  res.cookie('refresh_token', tokens.refreshToken, { httpOnly: true });
  return responseMapper({
    content: loginContentMapper(tokens),
    message: 'You have successfully signed in to HRM App',
  });
};
