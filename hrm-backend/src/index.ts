// import dotenv from 'dotenv';
import express, { json } from 'express';
import db from '../src/hrm-db/index.js';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import usersRouter from './routes/user-routes.js';
import authRoutes from './routes/auth-routes.js';

const PORT = process.env.PORT || 5000;

// dotenv.config();
const app = express();
const corsOptions = { credentials: true, origin: '*' };

// Middleware
app.use(cors(corsOptions));
app.use(json());
app.use(cookieParser());

app.use('/api', usersRouter);
app.use('/auth', authRoutes);

app.listen(PORT, () => {
  console.log(`Server has started on port ${PORT}`);
  db.runMigrations();
});
