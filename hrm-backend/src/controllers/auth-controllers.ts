import { loginUser } from './../services/auth-services.js';
import { NextFunction, Request, Response } from 'express';
import { createPassword } from '../services/auth-services.js';

const auth = {
  createPass: async (req: Request, res: Response, _next: NextFunction) => {
    try {
      res.json(await createPassword(req));
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  },
  login: async (req: Request, res: Response, _next: NextFunction) => {
    try {
      res.json(await loginUser(req, res));
    } catch (error) {
      console.error({ error: error.message });
      res.json({ error: error.message });
    }
  },
};

export default auth;
