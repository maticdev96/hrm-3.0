import { NextFunction, Request, Response } from 'express';
import { getAllUsers } from '../services/user-services.js';

const users = {
  get: async (_req: Request, res: Response, next: NextFunction) => {
    try {
      res.json(await getAllUsers(res));
    } catch (error) {
      console.error({ error: error.message });
      res.json({ error: error.message });
    }
  },
};

export default users;
