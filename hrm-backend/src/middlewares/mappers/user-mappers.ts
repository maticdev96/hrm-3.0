import {
  IGetAllUsersContent,
  IMapToResponse,
} from 'middlewares/interfaces/user-interfaces.js';

export const responseMapper = ({ content, message }: IMapToResponse) => ({
  content,
  message,
});

export const getAllUsersContentMapper = ({
  id,
  first_name,
  last_name,
  email,
  phone_number,
  access_token,
  password,
  role_id,
  created_at,
  last_updated_at,
  role_name,
}: IGetAllUsersContent) => {
  return {
    id,
    firstName: first_name,
    lastName: last_name,
    email,
    phoneNumber: phone_number,
    role: role_name,
    createdAt: created_at,
    lastUpdatedAt: last_updated_at,
  };
};
