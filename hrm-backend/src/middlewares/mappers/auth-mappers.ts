import { ILoggedInUser, ILoginContent } from '../interfaces/auth-interfaces.js';
import { IMapToResponse } from 'middlewares/interfaces/auth-interfaces.js';

export const responseMapper = ({ content, message }: IMapToResponse) => ({
  content,
  message,
});

export const loginContentMapper = ({
  accessToken,
  refreshToken,
}: ILoginContent) => ({
  accessToken,
  refreshToken,
});

export const createPassContentMapper = ([
  {
    id,
    first_name,
    last_name,
    email,
    phone_number,
    access_token,
    password,
    role_id,
    created_at,
    last_updated_at,
    role_name,
  },
]: Array<ILoggedInUser>) => {
  return {
    id,
    firstName: first_name,
    lastName: last_name,
    email,
    phoneNumber: phone_number,
    role: role_name,
  };
};
