export interface IMapToResponse {
  content: ILoginContent | ICreatePasswordContent | IUserMapped;
  message?: string;
}

export interface ILoginContent {
  accessToken: string;
  refreshToken: string;
}

export interface ICreatePasswordContent {
  password: string;
}

export interface ILoggedInUser {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  phone_number: string;
  access_token: string;
  password: string;
  role_id: number;
  created_at: string;
  last_updated_at: string;
  role_name: string;
}

export interface IUserMapped {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  role: string;
}
