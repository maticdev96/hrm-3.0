export interface IMapToResponse {
  content: Array<IGetAllUsersContent>;
  message?: string;
}

export interface IGetAllUsersContent {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  phone_number: string;
  access_token: string;
  password: string;
  role_id: number;
  created_at: string;
  last_updated_at: string;
  role_name: string;
}

export interface IMappedUserResponse {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  role: string;
  createdAt: string;
  lastUpdatedAt: string;
}
