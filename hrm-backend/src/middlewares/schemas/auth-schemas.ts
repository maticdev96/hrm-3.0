import Joi from 'joi';

export const loginSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'rs'] } })
    .required(),
  password: Joi.string()
    .pattern(new RegExp('^(?=.{6,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$'))
    .required(),
});

export const createPassSchema = Joi.object({
  password: Joi.string()
    .pattern(new RegExp('^(?=.{6,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$'))
    .required(),
});
