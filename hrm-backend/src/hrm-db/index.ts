import path, { dirname } from 'path';
import pkg from 'pg';
import { migrate } from 'postgres-migrations';
import { fileURLToPath } from 'url';

const { Pool } = pkg;

const poolConfig = {
  database: process.env.DATABASE,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  max: Number(process.env.DB_POOL_SIZE),
  idleTimeoutMillis: Number(process.env.DB_POOL_CLIENT_IDLE_TIMEOUT),
  connectionTimeoutMillis: Number(
    process.env.DB_POOL_CLIENT_CONNECTION_TIMEOUT
  ),
};

class Database {
  pool: any;

  constructor() {
    this.pool = new Pool(poolConfig);
  }

  runMigrations = async (): Promise<void> => {
    const __filename = fileURLToPath(import.meta.url);
    const __dirname = dirname(__filename);
    const client = await this.pool.connect();
    try {
      await migrate({ client }, path.resolve(__dirname, 'migrations/sql'));
    } catch (err) {
      console.error('migation failed', err);
    } finally {
      client.release();
    }
  };
}

const db = new Database();

export default db;
