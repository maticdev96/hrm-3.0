import { ReactNode } from 'react';
import { IAuthContentByType } from '../data/authInterfaces';
import {
  authContent,
  forgotComponents,
  loginComponents,
} from '../data/authData';
import { AuthContent } from '../AuthContent/AuthContent';

const login = () => <AuthContent formComponents={loginComponents} />;
const forgotPassword = () => <AuthContent formComponents={forgotComponents} />;
const createPassword = () => <AuthContent formComponents={loginComponents} />;

export const authTypeView: Record<
  'login' | 'forgot' | 'create',
  () => ReactNode
> = {
  login: login,
  forgot: forgotPassword,
  create: createPassword,
};

export const authContentByType: Record<
  'login' | 'forgot' | 'create',
  IAuthContentByType
> = {
  login: authContent.login,
  create: authContent.create,
  forgot: authContent.forgot,
};
