import { Center } from '@chakra-ui/react';
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import type { RootState } from '../../common/lib/redux/store';
import { Background } from '../../components/Background/Background';
import { ErrorBoundary } from '../../components/ErrorBoundary/ErrorBoundary';
import { authTypeView } from './helpers/authHelpers';
import { styleProps } from './style/styleProps';

type Props = {};

const Auth: React.FC = (props: Props) => {
  const { authCenter } = styleProps;
  const { type } = useSelector((state: RootState) => state.auth);
  const dispatch = useDispatch();
  return (
    <ErrorBoundary>
      <React.Suspense>
        <Background />
        <Center {...authCenter}>{authTypeView[type]()}</Center>
      </React.Suspense>
    </ErrorBoundary>
  );
};

export { Auth };
