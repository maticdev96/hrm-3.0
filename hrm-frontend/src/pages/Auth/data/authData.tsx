import { Input } from '@chakra-ui/react';
import { IoKey, IoMail } from 'react-icons/io5';
import { IconContext } from 'react-icons';
import { IAuthContentComponent, IAuthContentTypes } from './authInterfaces';

export const authContent: IAuthContentTypes = {
  login: {
    header: 'Sign in',
    description: 'Login to your HRM account.',
    buttonText: 'Sign in',
    footer: 'Forgot Password?',
    footerProps: 'forgot',
  },
  create: {
    header: 'Create your password',
    description:
      'When logging for the first time you need to create a password.',
    buttonText: 'Confirm password',
    footer: 'Go to Sign in page',
    footerProps: 'login',
  },
  forgot: {
    header: 'Forgot password?',
    description:
      "Please enter your registered email address. We'll send instructions to help reset your password.",
    buttonText: 'Send reset instructions',
    footer: 'Back to Sign in page',
    footerProps: 'login',
  },
};

// Login Data

export const loginComponents: Array<IAuthContentComponent> = [
  {
    id: 0,
    key: 'email',
    name: 'email',
    label: 'Email',
    placeholder: 'Email',
    component: Input,
    variant: 'outline',
    size: 'lg',
    type: 'email',
    prefix: (
      <IconContext.Provider
        value={{ style: { marginTop: '0.5em', width: '20px', height: '20px' } }}
      >
        <IoMail />
      </IconContext.Provider>
    ),
  },
  {
    id: 0,
    key: 'password',
    name: 'password',
    label: 'Password',
    placeholder: 'Password',
    component: Input,
    variant: 'outline',
    size: 'lg',
    type: 'password',
    props: {
      pr: '3.25em',
    },
    prefix: (
      <IconContext.Provider
        value={{ style: { marginTop: '0.5em', width: '20px', height: '20px' } }}
      >
        <IoKey />
      </IconContext.Provider>
    ),
  },
];

// Forgot Password Data

export const forgotComponents: Array<IAuthContentComponent> = [
  {
    id: 0,
    key: 'email',
    name: 'email',
    label: 'Email',
    placeholder: 'Email',
    component: Input,
    variant: 'outline',
    size: 'lg',
    type: 'email',
    prefix: (
      <IconContext.Provider
        value={{ style: { marginTop: '0.5em', width: '20px', height: '20px' } }}
      >
        <IoMail />
      </IconContext.Provider>
    ),
  },
];
