export interface IAuthContentComponent {
  id: number;
  key: string;
  name: string;
  label: string;
  placeholder: string;
  type: 'text' | 'email' | 'password';
  variant: 'outline' | 'filled' | 'flushed' | 'unstyled' | 'primary';
  component: any;
  size?: 'xs' | 'sm' | 'md' | 'lg';
  prefix?: any;
  iconRender?: any;
  props?: any;
}

export interface IAuthContentByType {
  header: string;
  description: string;
  buttonText: string;
  footer: string;
  footerProps: 'login' | 'forgot';
}

export interface IAuthContentTypes {
  login: IAuthContentByType;
  create: IAuthContentByType;
  forgot: IAuthContentByType;
}
