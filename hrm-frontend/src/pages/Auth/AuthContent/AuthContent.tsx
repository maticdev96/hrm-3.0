import {
  Box,
  Button,
  Heading,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Text,
  VStack,
} from '@chakra-ui/react';
import { motion } from 'framer-motion';
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import {
  authBoxAnimation,
  authContentAnimation,
  plusContentAnimation,
} from '../../../common/lib/framer.motion/authAnimations';
import { usePostLoginMutation } from '../../../common/lib/redux/slices/apiSlice';
import {
  setAuthType,
  togglePasswordVisibility,
  updateValues,
} from '../../../common/lib/redux/slices/authSlice';
import { RootState } from '../../../common/lib/redux/store';
import { ErrorBoundary } from '../../../components/ErrorBoundary/ErrorBoundary';
import { IAuthContentComponent } from '../data/authInterfaces';
import { authContentByType } from '../helpers/authHelpers';
import { styleProps } from './style/styleProps';

type Props = {
  formComponents: Array<IAuthContentComponent>;
};

const AuthContent = ({ formComponents }: Props) => {
  const {
    authBox,
    mainVStack,
    headVStack,
    heading,
    formVStack,
    headText,
    button,
    forgot,
  } = styleProps;
  const { type, showPassword, formValues } = useSelector(
    (state: RootState) => state.auth
  );
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [postLogin] = usePostLoginMutation();

  const handlePostLogin =
    (data: any, type: 'login' | 'forgot' | 'create') => () => {
      postLogin(data[type])
        .then((response: any) => {
          if (response?.data) {
            const token = response?.data?.content?.accessToken;
            localStorage.setItem('hrm_access_token', token);
            return navigate('/people');
          } else console.error('Something went wrong');
        })
        .catch((error) => console.error(error));
    };

  const PasswordAdornment = () => (
    <InputRightElement mt="0.25em" mr="0.8em" fontSize={'sm'}>
      <Text
        className="unselectable"
        onClick={() => dispatch(togglePasswordVisibility())}
        variant={'link'}
        as={'a'}
      >
        {showPassword ? 'Hide' : 'Show'}
      </Text>
    </InputRightElement>
  );
  return (
    <ErrorBoundary>
      <React.Suspense>
        <VStack>
          <Box as={motion.div} animation={authBoxAnimation} {...authBox}>
            <Box as={motion.div} animation={authContentAnimation}>
              <VStack {...mainVStack}>
                <VStack {...headVStack}>
                  <Heading {...heading}>
                    {authContentByType[type].header}
                  </Heading>
                  <Text {...headText}>
                    {authContentByType[type].description}
                  </Text>
                </VStack>
                <VStack {...formVStack}>
                  {formComponents.map((item) => (
                    <InputGroup mb="10px" key={item.key}>
                      <InputLeftElement
                        pointerEvents="none"
                        children={item.prefix}
                      />
                      {React.createElement(item.component, {
                        key: item.key,
                        variant: item.variant,
                        placeholder: item.placeholder,
                        type: item.type,
                        value: formValues[type][item.name],
                        onChange: (
                          event: React.ChangeEvent<HTMLInputElement>
                        ) =>
                          dispatch(
                            updateValues({
                              inputName: item.name,
                              inputValue: event?.target?.value,
                            })
                          ),
                        ...(item.size && { size: item.size }),
                        ...(item.props && { ...item.props }),
                        ...(item.type === 'password' && {
                          type: showPassword ? 'text' : item.type,
                        }),
                      })}
                      {item.type === 'password' && <PasswordAdornment />}
                    </InputGroup>
                  ))}
                  <Button
                    onClick={handlePostLogin(formValues, type)}
                    {...button}
                    variant={'primary'}
                  >
                    {authContentByType[type].buttonText}
                  </Button>
                </VStack>
              </VStack>
            </Box>
          </Box>
          <Text
            variant={'link'}
            as={motion.a}
            onClick={() =>
              dispatch(setAuthType(authContentByType[type].footerProps))
            }
            animation={plusContentAnimation}
            {...forgot}
          >
            {authContentByType[type].footer}
          </Text>
        </VStack>
      </React.Suspense>
    </ErrorBoundary>
  );
};

export { AuthContent };
