const styleProps = {
  authBox: {
    w: '500px',
    h: '440px',
    bgColor: '#fff',
    opacity: '0.7',
    blur: '2px',
    borderRadius: '3xl',
    boxShadow: '0 0 10px #fff',
  },
  mainVStack: {
    my: '60px',
    mx: '80px',
    w: 'calc(100% - 160px)',
  },
  headVStack: {
    mb: '20px',
    alignSelf: 'start',
    alignItems: 'start',
  },
  heading: {
    mb: '20px',
  },
  headText: {
    mt: '0 !important',
  },
  formVStack: {
    w: 'full',
  },
  button: {
    mt: '40px !important',
    w: 'full',
    h: '48px',
  },
  forgot: {
    zIndex: '1',
    mt: '60px !important',
    className: 'unselectable',
  },
};

export { styleProps };
