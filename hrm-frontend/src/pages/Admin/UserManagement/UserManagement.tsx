import * as React from 'react';
import { ErrorBoundary } from '../../../components/ErrorBoundary/ErrorBoundary';

type Props = {};

const UserManagement = (props: Props) => {
  return (
    <ErrorBoundary>
      <React.Suspense>UserManagement</React.Suspense>
    </ErrorBoundary>
  );
};

export { UserManagement };
