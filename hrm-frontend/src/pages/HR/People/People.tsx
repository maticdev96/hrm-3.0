import * as React from 'react';
import { ErrorBoundary } from '../../../components/ErrorBoundary/ErrorBoundary';

type Props = {};

const People = (props: Props) => {
  return (
    <ErrorBoundary>
      <React.Suspense>People</React.Suspense>
    </ErrorBoundary>
  );
};

export { People };
