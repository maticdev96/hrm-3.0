import * as React from 'react';
import { ErrorBoundary } from '../../../components/ErrorBoundary/ErrorBoundary';

type Props = {};

const Clients = (props: Props) => {
  return (
    <ErrorBoundary>
      <React.Suspense>Clients</React.Suspense>
    </ErrorBoundary>
  );
};

export { Clients };
