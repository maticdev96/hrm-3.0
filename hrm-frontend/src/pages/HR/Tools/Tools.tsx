import * as React from 'react';
import { ErrorBoundary } from '../../../components/ErrorBoundary/ErrorBoundary';

type Props = {};

const Tools = (props: Props) => {
  return (
    <ErrorBoundary>
      <React.Suspense>Tools</React.Suspense>
    </ErrorBoundary>
  );
};

export { Tools };
