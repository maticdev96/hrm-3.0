import { checkTokenRole } from '../utils/checkTokenRole';

const useWhatRole = () => {
  const whatRole = { role: checkTokenRole() };
  return whatRole && whatRole.role;
};

export { useWhatRole };
