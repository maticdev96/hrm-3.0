import { getLoggedUserInfo } from '../utils/getLoggedUserInfo';

const useLoggedUser = () => {
  const userInfo = getLoggedUserInfo();
  return userInfo && userInfo;
};

export { useLoggedUser };
