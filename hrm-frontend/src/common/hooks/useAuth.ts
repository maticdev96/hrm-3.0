import { checkTokenValidity } from '../utils/checkTokenValidity';

const useAuth = () => {
  const user = { loggedIn: checkTokenValidity() };
  return user && user.loggedIn;
};

export default useAuth;
