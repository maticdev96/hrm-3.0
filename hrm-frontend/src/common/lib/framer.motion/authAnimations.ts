import { keyframes } from '@emotion/react';

const boxKeyframes = keyframes`
  from { transform: scale(0) ; opacity: 0}
  to {transform: scale(1); opacity: 0.7}
`;

const contentKeyframes = keyframes`
  0% { opacity: 0}
  50% {opacity: 0}
  100% {opacity: 0.7}
`;

const plusContentKeyframes = keyframes`
  0% { opacity: 0}
  50% {opacity: 0}
  100% {opacity: 0.7}
`;

export const authBoxAnimation = `${boxKeyframes} 0.5s ease-in-out`;
export const authContentAnimation = `${contentKeyframes} 1s ease-in-out`;
export const plusContentAnimation = `${contentKeyframes} 1.5s ease-in-out`;
