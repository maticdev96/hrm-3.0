import { ComponentStyleConfig } from '@chakra-ui/react';
import { mode } from '@chakra-ui/theme-tools';

export const ButtonStyles: ComponentStyleConfig = {
  baseStyle: {},
  sizes: {},
  variants: {
    primary: (props) => ({
      bg: 'primary',
      color: '#fff',
      _hover: { bg: mode('primaryDarken', 'primaryWhiten')(props) },
      boxShadow: 'md',
      _active: { bg: mode('primaryHeavyDarken', 'primaryHeavyWhiten')(props) },
      borderRadius: 'xl',
    }),
  },
};
