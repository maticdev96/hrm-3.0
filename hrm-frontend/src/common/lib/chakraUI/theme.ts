import '@fontsource/montserrat';
import '@fontsource/roboto';
import { theme as chakraTheme } from '@chakra-ui/react';
import { extendTheme } from '@chakra-ui/react';
import { InputStyles as Input } from './componentsStyles/InputStyles';
import { ButtonStyles as Button } from './componentsStyles/ButtonStyles';
import { TextStyles as Text } from './componentsStyles/TextStyles';

const theme = extendTheme({
  colors: {
    primary: '#009DE9',
    primaryDarken: '#007eba',
    primaryHeavyDarken: '#005e8c',
    primaryWhiten: '#33b1ed',
    primaryHeavyWhiten: '#66c4f2',
    secondary: '#404C66',
    secondaryDarken: '#333d52',
    secondaryHeavyDarken: '#262e3d',
    secondaryWhiten: '#667085',
    secondaryHeavyWhiten: '#8c94a3',
  },
  fonts: {
    ...chakraTheme.fonts,
    heading: `Montserrat, Roboto, sans-serif, Segoe UI`,
    body: `Montserrat, Roboto, sans-serif, Segoe UI`,
  },
  components: {
    Input,
    Button,
    Text,
  },
});

export { theme };
