import jwt from 'jwt-decode';

const getLoggedUserInfo = () => {
  const token = localStorage.getItem('hrm_access_token');
  if (token) {
    try {
      const decodedToken: any = jwt(token);
      return decodedToken;
    } catch (error) {
      console.error(error);
    }
  }
  return false;
};

export { getLoggedUserInfo };
