import { lazy } from 'react';
import { Navigate } from 'react-router-dom';

const Auth = lazy(() =>
  import('../../pages/Auth/Auth').then(({ Auth }) => ({ default: Auth }))
);
const UserManagement = lazy(() =>
  import('../../pages/Admin/UserManagement/UserManagement').then(
    ({ UserManagement }) => ({
      default: UserManagement,
    })
  )
);
const People = lazy(() =>
  import('../../pages/HR/People/People').then(({ People }) => ({
    default: People,
  }))
);
const Clients = lazy(() =>
  import('../../pages/HR/Clients/Clients').then(({ Clients }) => ({
    default: Clients,
  }))
);
const Tools = lazy(() =>
  import('../../pages/HR/Tools/Tools').then(({ Tools }) => ({
    default: Tools,
  }))
);

export const routes = (isAuth: boolean, role: string) => [
  {
    path: '/',
    element: isAuth ? <Navigate to="/people" /> : <Navigate to="/auth" />,
  },
  { path: 'auth', element: <Auth /> },
  {
    path: 'user-management',
    element:
      isAuth && role === 'admin' ? <UserManagement /> : <Navigate to="/auth" />,
  },
  { path: 'people', element: isAuth ? <People /> : <Navigate to="/auth" /> },
  { path: 'clients', element: isAuth ? <Clients /> : <Navigate to="/auth" /> },
  { path: 'tools', element: isAuth ? <Tools /> : <Navigate to="/auth" /> },
  { path: '*', element: <Navigate to="/" /> },
];
