import * as React from 'react';
import { Layout } from '../components/Layout/Layout';
import logo from '../assets/brand/ates-soft-logo.png';
import { useWhatRole } from '../common/hooks/useWhatRole';
import { loggedUserMapper } from '../components/Layout/helpers/layoutHelpers';
import { useLoggedUser } from '../common/hooks/useLoggedUser';

const ProtectedPages = ({ element }: any) => {
  const role = useWhatRole();
  const loggedUser = loggedUserMapper(useLoggedUser());
  const { Header, Sidebar, Content } = Layout;
  return (
    <Layout>
      <Sidebar logo={logo} loggedUser={loggedUser}></Sidebar>
      <Header loggedUser={loggedUser} logo={logo} />
      <Content>{element}</Content>
    </Layout>
  );
};

export { ProtectedPages };
