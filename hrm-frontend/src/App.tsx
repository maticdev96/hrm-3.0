import * as React from 'react';
import { useLocation, useRoutes } from 'react-router-dom';
import useAuth from './common/hooks/useAuth';
import { useWhatRole } from './common/hooks/useWhatRole';
import { routes } from './common/routing/routes';
import { ProtectedPages } from './features/ProtectedPages';

const App: React.FC = () => {
  const location = useLocation();
  const isAuth = useAuth();
  const role = useWhatRole();

  let element = useRoutes(routes(isAuth, role));
  return location.pathname === '/auth' || !isAuth ? (
    <>{element}</>
  ) : (
    <ProtectedPages element={element} />
  );
};

export { App };
