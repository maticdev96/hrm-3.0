import {
  Box,
  CloseButton,
  Flex,
  useColorModeValue,
  Drawer,
  DrawerContent,
  useDisclosure,
  BoxProps,
  Image,
} from '@chakra-ui/react';
import { FiSettings, FiUsers, FiCode } from 'react-icons/fi';
import { FaRegHandshake, FaRegNewspaper } from 'react-icons/fa';
import { IconType } from 'react-icons';
import { MobileNav } from '../MobileNav/MobileNav';
import { NavItem } from './components/NavItem';

interface ILinkItemProps {
  name: string;
  icon: IconType;
  path: string;
}

const LinkItems: Array<ILinkItemProps> = [
  { name: 'News Feed', icon: FaRegNewspaper, path: '/news-feed' },
  { name: 'People', icon: FiUsers, path: '/people' },
  { name: 'Clients', icon: FaRegHandshake, path: '/clients' },
  { name: 'Workplaces', icon: FiCode, path: '/workplaces' },
  { name: 'Settings', icon: FiSettings, path: '/settings' },
];

type Props = {};

const Sidebar = (props: any) => {
  const { logo, loggedUser } = props;
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Box>
      <SidebarContent
        logo={logo}
        onClose={() => onClose}
        display={{ base: 'none', md: 'block' }}
      />
      <Drawer
        autoFocus={false}
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <SidebarContent onClose={onClose} />
        </DrawerContent>
      </Drawer>
      {/* mobilenav */}
      <Box display={{ base: 'block', md: 'none' }}>
        <MobileNav onOpen={onOpen} logo={logo} loggedUser={loggedUser} />
      </Box>
      {/* {children} */}
    </Box>
  );
};

interface ISidebarProps extends BoxProps {
  onClose: () => void;
  logo?: string;
}

const SidebarContent = ({ onClose, ...rest }: ISidebarProps) => {
  const { logo } = rest;
  return (
    <Box
      transition="3s ease"
      bg={useColorModeValue('secondaryHeavyWhiten', 'secondary')}
      borderRightColor={useColorModeValue('gray.200', 'gray.700')}
      w={{ base: 'full', md: 60 }}
      pos="fixed"
      h="full"
      {...rest}
    >
      <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
        <Image src={logo} />
        <CloseButton display={{ base: 'flex', md: 'none' }} onClick={onClose} />
      </Flex>
      {LinkItems.map((link) => (
        <NavItem key={link.name} icon={link.icon} path={link.path}>
          {link.name}
        </NavItem>
      ))}
    </Box>
  );
};

export { Sidebar };
