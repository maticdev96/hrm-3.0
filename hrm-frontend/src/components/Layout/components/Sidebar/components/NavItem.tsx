import { Flex, FlexProps, Icon } from '@chakra-ui/react';
import { IconType } from 'react-icons';
import { Link, useLocation } from 'react-router-dom';

interface IProps extends FlexProps {
  icon: IconType;
  children: any;
  path: string;
}

const NavItem = ({ icon, path, children, ...rest }: IProps) => {
  const location = useLocation();
  return (
    <Link
      to={path}
      style={{
        textDecoration: 'none',
        pointerEvents: `${location.pathname === path ? 'none' : 'auto'}`,
      }}
    >
      <Flex
        align="center"
        p="3"
        m="2"
        borderRadius="xl"
        role="group"
        cursor="pointer"
        color="white"
        transition="all 1s ease"
        _hover={{
          bgGradient:
            'linear(to-r, rgba(254,254,254,0.5) 0%, rgba(254,254,254,0.05) 100%)',
          transition: 'all 1s ease',
        }}
        _active={{
          bgGradient:
            'linear(to-r, rgba(254,254,254,0.5) 0%, rgba(254,254,254,0.15) 100%)',
          transition: 'all 1s ease',
        }}
        bgGradient={
          location.pathname === path
            ? 'linear(to-r, rgba(254,254,254,0.5) 0%, rgba(254,254,254,0.15) 100%)'
            : ''
        }
        {...rest}
      >
        {icon && (
          <Icon
            mr="4"
            fontSize="16"
            _groupHover={{
              color: 'white',
            }}
            as={icon}
          />
        )}
        {children}
      </Flex>
    </Link>
  );
};

export { NavItem };
