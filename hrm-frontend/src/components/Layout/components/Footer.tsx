import { GridItem } from '@chakra-ui/react';
import * as React from 'react';

type Props = {};

const Footer = (props: Props) => {
  return (
    <GridItem pl="2" bg="green.300" area={'footer'}>
      Footer
    </GridItem>
  );
};

export { Footer };
