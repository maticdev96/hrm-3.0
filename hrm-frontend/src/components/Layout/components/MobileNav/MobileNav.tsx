import {
  Avatar,
  Box,
  Flex,
  FlexProps,
  HStack,
  IconButton,
  Image,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
  Text,
  useColorModeValue,
  VStack,
} from '@chakra-ui/react';
import { FiBell, FiChevronDown, FiMenu } from 'react-icons/fi';
import { ILoggedUser } from '../../data/layoutInterfaces';

interface IProps extends FlexProps {
  onOpen: () => void;
  loggedUser: ILoggedUser;
  logo?: string;
}

const MobileNav = ({ onOpen, ...rest }: IProps) => {
  const { logo, loggedUser } = rest;
  return (
    <Flex
      height="20"
      alignItems="center"
      bg={useColorModeValue('rgba(255,255,255,0.8)', 'gray.900')}
      justifyContent={{ base: 'space-between', md: 'flex-end' }}
      // {...rest}
    >
      <IconButton
        display={{ base: 'flex', md: 'none' }}
        onClick={onOpen}
        variant="outline"
        aria-label="open menu"
        icon={<FiMenu />}
      />
      <Image display={{ base: 'block', md: 'none' }} src={logo} />
      <HStack spacing={{ base: '0', md: '6' }}>
        <IconButton
          size="lg"
          variant="ghost"
          aria-label="open menu"
          icon={<FiBell />}
        />
        <Flex alignItems={'center'}>
          <Menu>
            <MenuButton
              mr="4"
              py={2}
              transition="all 0.3s"
              _focus={{ boxShadow: 'none' }}
            >
              <HStack>
                <Avatar size={'sm'} />
                <VStack
                  display={{ base: 'none', md: 'flex' }}
                  alignItems="flex-start"
                  spacing="1px"
                  mx="3 !important"
                >
                  <Text fontSize="sm">{`${loggedUser?.firstName}  ${loggedUser?.lastName}`}</Text>
                  <Text fontSize="xs" color="gray.600">
                    {loggedUser?.role}
                  </Text>
                </VStack>
                <Box display={{ base: 'none', md: 'flex' }}>
                  <FiChevronDown />
                </Box>
              </HStack>
            </MenuButton>
            <MenuList
              bg={useColorModeValue('white', 'gray.900')}
              borderColor={useColorModeValue('gray.200', 'gray.700')}
            >
              <MenuItem>Profile</MenuItem>
              <MenuItem>Settings</MenuItem>
              <MenuItem>Billing</MenuItem>
              <MenuDivider />
              <MenuItem>Sign out</MenuItem>
            </MenuList>
          </Menu>
        </Flex>
      </HStack>
    </Flex>
  );
};

export { MobileNav };
