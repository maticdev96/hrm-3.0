import { Box, useDisclosure } from '@chakra-ui/react';
import { ILoggedUser } from '../../data/layoutInterfaces';
import { MobileNav } from '../MobileNav/MobileNav';

type Props = {
  logo?: string;
  loggedUser: ILoggedUser;
  children: any;
};

const Header = (props: Props) => {
  const { logo, loggedUser } = props;
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Box display={{ base: 'none', md: 'block' }}>
      <MobileNav onOpen={onOpen} logo={logo} loggedUser={loggedUser} />
    </Box>
  );
};

export { Header };
