import { Box, GridItem } from '@chakra-ui/react';
import * as React from 'react';

type Props = {};

const Content = ({ children }: any) => {
  return (
    <Box ml={{ base: 0, md: 60 }} p="5">
      {children}
    </Box>
  );
};

export { Content };
