import { Box } from '@chakra-ui/react';
import * as React from 'react';
import { Content } from './components/Content/Content';
import { Footer } from './components/Footer';
import { Header } from './components/Header/Header';
import { Sidebar } from './components/Sidebar/Sidebar';

class Layout extends React.Component {
  static Header = ({ children, ...props }: any) => (
    <Header {...props}>{children}</Header>
  );
  static Sidebar = ({ children, ...props }: any) => (
    <Sidebar {...props}>{children}</Sidebar>
  );
  static Content = ({ children, ...props }: any) => (
    <Content {...props}>{children}</Content>
  );
  static Footer = ({ children, ...props }: any) => (
    <Footer {...props}>{children}</Footer>
  );
  render() {
    const { children }: any = this.props;
    return (
      <Box
        minH="100vh"
        bgGradient="linear(to-tr, #fefefe, #edecf9, #dadbf4, #c4cbf0, #acbbec, #9cb3eb, #8aace9, #75a5e8, #66a3e9, #53a1e9, #3a9fe9, #009de9)"
      >
        {typeof children === 'function' ? children() : children}
      </Box>
    );
  }
}
export { Layout };
