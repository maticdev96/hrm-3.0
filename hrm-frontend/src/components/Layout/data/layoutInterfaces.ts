export interface ILoggedUser {
  id: string | number;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  role: string;
  createdAt: string;
  lastUpdatedAt: string;
  exp?: number;
  iat?: number;
}
