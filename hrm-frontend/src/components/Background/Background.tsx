import * as React from 'react';
import { Box, Image } from '@chakra-ui/react';
import Logo from '../../assets/brand/ates-soft-logo.png';

const styleProps = {
  background: {
    w: '100vw',
    h: '100vh',
    bgGradient:
      'linear(to-tr, #fefefe, #edecf9, #dadbf4, #c4cbf0, #acbbec, #9cb3eb, #8aace9, #75a5e8, #66a3e9, #53a1e9, #3a9fe9, #009de9)',
    overflow: 'hidden',
    sx: {
      position: 'fixed',
    },
  },
  logo: {
    src: Logo,
    w: '200px',
    mt: '80px',
    mx: 'auto',
  },
};

const Background = () => {
  return (
    <Box {...styleProps.background}>
      <Image {...styleProps.logo} />
    </Box>
  );
};

export { Background };
